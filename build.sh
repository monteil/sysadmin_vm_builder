#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail


# Variables you may want to adapt
export TARGET_DIR='/run/shm/sysadmin_vm'
export TARGET_FILENAME_PREFIX='debian_12_sysadmin'
export TARGET_RAM='4096'
export TARGET_CPUS='4'
export TARGET_DISK_SIZE='20G'
export TARGET_OS_TYPE='Debian_64'
export ISO_URL='./debian-12.5.0-amd64-netinst.iso'
export ISO_URL='https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.5.0-amd64-netinst.iso'
export ISO_CHECKSUM='sha512:33c08e56c83d13007e4a5511b9bf2c4926c4aa12fd5dd56d493c0653aecbab380988c5bf1671dbaea75c582827797d98c4a611f7fb2b131fbde2c677d5258ec9'


rm -rf ${TARGET_DIR}

packer build sysadmin.pkr.hcl

# shrink the qcow2 image
qemu-img convert -c -f qcow2 "${TARGET_DIR}/tmp.${TARGET_FILENAME_PREFIX}.qcow2" -O qcow2 "${TARGET_DIR}/${TARGET_FILENAME_PREFIX}.qcow2"
rm "${TARGET_DIR}/tmp.${TARGET_FILENAME_PREFIX}.qcow2"

# creating the OVA from the qcow2 image
qemu-img convert -f qcow2 "${TARGET_DIR}/${TARGET_FILENAME_PREFIX}.qcow2" -O vdi "${TARGET_DIR}/${TARGET_FILENAME_PREFIX}.vdi"
vboxmanage createvm --name "${TARGET_FILENAME_PREFIX}" --ostype "${TARGET_OS_TYPE}" --register --basefolder "${TARGET_DIR}"
vboxmanage modifyvm "${TARGET_FILENAME_PREFIX}" --cpus "${TARGET_CPUS}" --memory "${TARGET_RAM}" --vram "128"
vboxmanage modifyvm "${TARGET_FILENAME_PREFIX}" --graphicscontroller "vmsvga"
vboxmanage modifyvm "${TARGET_FILENAME_PREFIX}" --nic1 "nat"
vboxmanage storagectl "${TARGET_FILENAME_PREFIX}" --name "SATA Controller" --add "sata" --bootable "on"
vboxmanage storageattach "${TARGET_FILENAME_PREFIX}" --storagectl "SATA Controller" --port "0" --device "0" --type "hdd" --medium "${TARGET_DIR}/${TARGET_FILENAME_PREFIX}.vdi"
vboxmanage export "${TARGET_FILENAME_PREFIX}" --output "${TARGET_DIR}/${TARGET_FILENAME_PREFIX}.ova"
vboxmanage unregistervm "${TARGET_FILENAME_PREFIX}" --delete-all

