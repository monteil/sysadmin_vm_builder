# packer config file

variable "target_dir" {
  type = string
  default = "${env("TARGET_DIR")}"
}

variable "target_filename_prefix" {
  type = string
  default = "${env("TARGET_FILENAME_PREFIX")}"
}

variable "target_cpus" {
  type = string
  default = "${env("TARGET_CPUS")}"
}

variable "target_ram" {
  type = string
  default = "${env("TARGET_RAM")}"
}

variable "target_disk_size" {
  type = string
  default = "${env("TARGET_DISK_SIZE")}"
}

variable "user_iso_checksum" {
  type = string
  default = "${env("ISO_CHECKSUM")}"
}

variable "user_iso_url" {
  type = string
  default = "${env("ISO_URL")}"
}

source "qemu" "sysadmin" {
  boot_command      = ["<esc><wait><wait>", "install ", "auto=true ", "priority=critical ", "interface=auto ", "url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/sysadmin.preseed", "<enter>"]
  http_directory    = "."
  cpus              = "${var.target_cpus}"
  format            = "qcow2"
  disk_size         = "${var.target_disk_size}"
  memory            = "${var.target_ram}"
  headless          = false
  iso_checksum      = "${var.user_iso_checksum}"
  iso_url           = "${var.user_iso_url}"
  output_directory  = "${var.target_dir}"
  vm_name           = "tmp.${var.target_filename_prefix}.qcow2"
  # qemuargs          = [["-display", "gtk"]]
  qemuargs         = [["-display", "none"]]
  shutdown_command  = "echo 'insecure'|sudo -S shutdown -h now"
  shutdown_timeout  = "20m"
  communicator      = "none"
}

build {
  sources = ["sources.qemu.sysadmin"]
}

