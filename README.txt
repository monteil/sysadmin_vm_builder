Quick VM builder for sysadmin lecture
=====================================

This small script builds VM images (for qemu and virtualbox) for a sysadmin
course.

It aims to be as basic as possible. In particular, all the configuration is
done through the preseed file (no SSH server to provision the VM are
installed).

The OVA image is built from the qcow2 image because we were not able to let the
virtualbox packer plugin work.


Dependencies
------------

This script is tested on Debian 12 (bookworm). It relies on the following
packages:

- packer
- qemu-utils
- virtualbox (from Debian fasttrack)


Configure
---------

Some variables (paths and names) are defined at the beginning of the build.sh
script.

Installed packages and deployment scripts are defined in the sysadmin.preseed
file. This file is obtained from the official Debian preseed file, with
explicit changes to ease updates w.r.t new Debian releases.


Build
-----

If you build in RAM (which is the default), you might want to enlarge /run/shm
with:

  # mount -o remount,size=30G /run/shm/

To build images, just execute:

  # ./build.sh

The images will be found in the /run/shm/sysadmin_vm directory (by default).


Test
----

You can test the qemu image with:

  $ qemu-system-x86_64 -m 4G -display gtk /run/shm/sysadmin_vm/debian_12_sysadmin.qcow2

You can test the ova image with:

  $ vboxmanage startvm /run/shm/sysadmin_vm/debian_12_sysadmin.ova


Licence
-------

This software is released under the AGPL-3.0 licence.

